FROM ubuntu:20.04
MAINTAINER Tinamica_MLOps

#Not necessary if you set it at run time: docker run ... -p NNNN:NNNN
#ENV PORT 5000

WORKDIR /usr/src

RUN apt-get update
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN apt-get install vim -y

RUN pip install requests
RUN pip install pandas
RUN pip install flask
RUN pip install flask-restful
#RUN pip install -U flask-cors
RUN pip install flask-cors
RUN pip install flasgger

#RUN pip install nltk
#RUN pip install marisa-trie
#RUN pip install spanish_sentiment_analysis
#RUN pip install h5py
#RUN pip install keras
#RUN pip install librosa

#RUN pip install Werkzeug

#RUN pip install spacy
#RUN python -m spacy download es
#RUN python -m spacy download en

#RUN pip install shapely
#RUN pip install pyproj

#RUN pip install ipython
#RUN pip install matplotlib

#RUN pip install strsim

#Not necessary if you set it at run time: docker run ... -p NNNN:NNNN
#EXPOSE 5000

#CMD ["python3", "/usr/src/server.py"]
ENTRYPOINT ["/usr/src/entrypoint.sh"]

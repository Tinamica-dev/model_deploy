from os import environ, path

basedir = path.abspath(path.dirname(__file__))


class Config(object):
    DEBUG = False
    DEVELOPMENT = False
#    SECRET_KEY = environ.get('SECRET_KEY') or \
#        '\xfd{H\xe5<\x95\xf9\xe3\x96.5\xd1\x01O<!\xd5\xa2\xa0\x9fR"\xa1\xa8'
#    FLASK_SECRET = SECRET_KEY


class DevConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
#    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI') or \
#        'sqlite:///' + os.path.join(basedir, 'db.sqlite')
    

class ProdConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
#    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI') or \
#        'sqlite:///' + os.path.join(basedir, 'db.sqlite')

    
config = {
    'dev': DevConfig,
    'prod': ProdConfig,
    'default': DevConfig,
}
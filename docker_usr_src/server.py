import json,os
import logging
import sys
import config
import requests
from flask import Flask, request, Response, jsonify
from flask_restful import Api, Resource, reqparse
from flasgger import Swagger, swag_from


# Setup Flask Server
app = Flask(__name__)

 
# Create an APISpec
template = {
  "swagger": "2.0",
  "info": {
    "title": "Flask Restful Swagger Demo",
    "description": "A Demof for the Flask-Restful Swagger Demo",
    "version": "0.1.1",
    "contact": {
      "name": "Kanoki",
      "url": "https://Kanoki.org",
    }
  },
  "securityDefinitions": {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header",
      "description": "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\""
    }
  },
  "security": [
    {
      "Bearer": [ ]
    }
  ]

}

app.config['SWAGGER'] = {
    'title': 'My API',
    'uiversion': 3,
    "specs_route": "/swagger/"
}
swagger = Swagger(app, template= template)
app.config.from_object(config.Config)
api = Api(app)

class Todo(Resource):
    def get(self):
      """
      post endpoint
      ---      
      tags:
        - Flast Restful APIs
      parameters:
        - name: a
          in: query
          type: integer
          required: true
          description: first number
        - name: b
          in: query
          type: integer
          required: true
          description: second number
      responses:
        500:
          description: Error The number is not integer!
        200:
          description: Number statistics
          schema:
            id: stats
            properties:
              sum:
                type: integer
                description: The sum of number
              product:
                type: integer
                description: The product of number
              division:
                type: integer
                description: The division of number              
      """
      
      a = int(request.args.get("a"))
      b = int(request.args.get("b"))
      numsum = a+b
      prod = a*b
      div = a/b
      return jsonify({
                "sum": numsum,
                "product": prod,
                "division": div
            })
        


## Api resource routing
api.add_resource(Todo, '/stats')


if __name__ == "__main__":
    myPort = os.environ['PORT']
    print("PORT: " + myPort + " (received at " + __file__ + ")")
    print("YOU CAN NOW ACCESS SWAGGER AT: http://localhost:" + myPort + "/swagger")
    app.run(host='0.0.0.0', port=myPort, debug=True)
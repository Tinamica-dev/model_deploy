@echo off
SET myPort=%1%
if "%myPort%"=="" (
	SET myPort=5000
	ECHO Setting por to default! You can specify it as a parameter
)

echo PORT: %myPort% (set at %0)
docker run --rm --name model_deploy -i -t -p %myPort%:%myPort% -e PORT=%myPort% -v %cd%\docker_usr_src:/usr/src model_deploy
